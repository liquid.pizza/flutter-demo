import 'package:flutter/material.dart';

class InfoCardData
{
  String title;
  String infoText;

  InfoCardData({
    this.title,
    this.infoText: "",
  });

  factory InfoCardData.fromJson(Map<String, dynamic> json) {
    return InfoCardData (
          title: json["title"],
          infoText: json["infoText"],
        );
  }
}

class InfoCard extends StatelessWidget
{

  final InfoCardData data;

  InfoCard({
    this.data,
  });

  Widget build(BuildContext context) {
    return Card (
      child: Column (
        children: <Widget>[
          Text (
            this.data.title,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Container (
            color: Colors.orange,
            child: Row (
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Icon(
                //   Icons.weekend,
                // ),
                Flexible (
                  child: Text(
                    this.data.infoText,
                    style: TextStyle(
                      color: Colors.white,
                      fontStyle: FontStyle.italic,
                    )
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

}