import 'dart:convert';

import 'package:flutter/material.dart';
import 'globals.dart' as globals;

class AddDialog extends StatefulWidget {

  @override
  _AddDialogState createState() => _AddDialogState();
}

class _AddDialogState extends State<AddDialog> {
  final _titleTextController = TextEditingController();
  final _infoTextTextController = TextEditingController();

  double _formProgress = 0;

  void _updateFormProgress() {
    var progress = 0.0;
    var controllers = [
      _titleTextController,
      _infoTextTextController,
    ];

    for (var controller in controllers) {
      if (controller.value.text.isNotEmpty) {
        progress += 1 / controllers.length;
      }
    }

    setState(() {
      _formProgress = progress;
    });
  }

  void cancleInput(BuildContext context) {
    Navigator.of(context).pop();
  }

  void addInput(BuildContext context) {

    globals.httpHandler.postData(
      _titleTextController.text,
      _infoTextTextController.text
    );
    
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Material (
      color: Colors.white,
      child: Form(
        onChanged: _updateFormProgress,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              LinearProgressIndicator(value: _formProgress),
              Text('Add new DATA', style: Theme
                  .of(context)
                  .textTheme
                  .headline4),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _titleTextController,
                  decoration: InputDecoration(hintText: "Title"),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _infoTextTextController,
                  decoration: InputDecoration(hintText: "Info text"),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FlatButton(
                    color: Colors.orange,
                    onPressed: () => cancleInput(context),
                    child: Text('Cancle'),
                  ),
                  FlatButton(
                    color: Colors.orange,
                    onPressed: _formProgress == 1 ? () => addInput(context) : null,
                    child: Text('Add'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}