import 'package:flutter/material.dart';
import 'package:flutter_demo/infoCard.dart';

class CardList extends StatelessWidget {

  final List<InfoCardData> entries;

  CardList ({
    this.entries,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.topCenter,
        child: SizedBox(
          width: 400,
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: entries.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: InfoCard(
                  data: this.entries[index],
                ),
              );
            }
          ),
        ),
      );
  }
}