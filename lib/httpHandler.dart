import 'dart:async';
import 'dart:convert';
import 'package:flutter_demo/infoCard.dart';
import 'package:http/http.dart' as http;

class HttpHandler {

  String _POST_URL = "http://pop-os.lan:8080/post";
  String _GET_URL = "http://pop-os.lan:8080/get";

  Future<List<InfoCardData>> getData() async {

    String url = _GET_URL;

    final response = await http.get(url);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List<InfoCardData> ret = [];

      print(response.body);
      
      var jsonDec = json.decode(response.body);

      for (var i=0; i < jsonDec.length; i++)
      {
        ret.add(InfoCardData.fromJson(jsonDec[i]));
      }

      return ret;

    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load data.');
    }

  }

  Future<List<InfoCardData>> postData(
    String title,
    String infoText) async {
    final http.Response response = await http.post(
      "http://pop-os.lan:8080/post",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "title": title,
        "infoText": infoText,
      }),
    );

    if (response.statusCode == 201) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
        List<InfoCardData> ret = [];

        print(response.body);
        
        var jsonDec = json.decode(response.body);

        for (var i=0; i < jsonDec.length; i++)
        {
          ret.add(InfoCardData.fromJson(jsonDec[i]));
        }

        return ret;
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to load data.');
    }
}

}